package org.strategywiki.json;


import org.junit.Test;
import org.strategywiki.dataclient.WikiSite;
import org.strategywiki.history.HistoryEntry;
import org.strategywiki.page.PageBackStackItem;
import org.strategywiki.page.PageTitle;
import org.strategywiki.page.tabs.Tab;

import java.util.Date;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;

// PageTitle.PageProperties.Location uses a Bundle which is incompatible with Robolectric
public class TabUnmarshallerTest {
    @Test public void testUnmarshalNull() {
        assertThat(TabUnmarshaller.unmarshal(null), empty());
    }

    @Test public void testUnmarshalSingle() {
        PageTitle page = new PageTitle("text", WikiSite.forLanguageCode("test"));
        HistoryEntry history = new HistoryEntry(page, new Date(0), HistoryEntry.SOURCE_SEARCH);
        Tab tab = new Tab();
        tab.getBackStack().add(new PageBackStackItem(page, history));
        List<Tab> tabs = singletonList(tab);

        assertThat(TabUnmarshaller.unmarshal(GsonMarshaller.marshal(tabs)), is(tabs));
    }

    // T152980
    @Test(expected = RuntimeException.class) public void testUnmarshalNoPageTitleAuthority() {
        PageTitle page = new PageTitle("text", new WikiSite("", ""));
        HistoryEntry history = new HistoryEntry(page, new Date(0), HistoryEntry.SOURCE_SEARCH);
        Tab tab = new Tab();
        tab.getBackStack().add(new PageBackStackItem(page, history));
        List<Tab> tabs = singletonList(tab);

        TabUnmarshaller.unmarshal(GsonMarshaller.marshal(tabs));
    }

    // T152980
    @Test(expected = RuntimeException.class) public void testUnmarshalNoHistoryEntryAuthority() {
        PageTitle page = new PageTitle("text", WikiSite.forLanguageCode("test"));
        PageTitle prevPage = new PageTitle("text", new WikiSite("", ""));
        HistoryEntry history = new HistoryEntry(prevPage, new Date(0), HistoryEntry.SOURCE_SEARCH);
        Tab tab = new Tab();
        tab.getBackStack().add(new PageBackStackItem(page, history));
        List<Tab> tabs = singletonList(tab);

        TabUnmarshaller.unmarshal(GsonMarshaller.marshal(tabs));
    }
}
