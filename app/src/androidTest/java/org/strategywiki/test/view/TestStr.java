package org.strategywiki.test.view;

import android.support.annotation.StringRes;

public interface TestStr {
    @StringRes int id();
    boolean isNull();
}
