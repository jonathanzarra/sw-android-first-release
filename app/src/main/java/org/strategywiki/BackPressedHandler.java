package org.strategywiki;

public interface BackPressedHandler {
    boolean onBackPressed();
}
