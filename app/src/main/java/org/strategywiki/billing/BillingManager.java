package org.strategywiki.billing;

import java.util.HashMap;
import java.util.Map;
import android.preference.PreferenceManager;
import android.content.Context;
import android.content.SharedPreferences;

public class BillingManager {
    public static final String PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA2" +
            "o+d7J2jo2uao/2vKuH0DQOPLf6dNt2EYBNja7dUO3o1gssbGolo9EHN5X/InYBIuuH7Zzar/ETNBjR" +
            "djGjZa/r56luwlisI4VmHD93HyuyuaiivUo4tQxk9qQto9xheSRBpKsuRVLAO94wb2+sOImmrNaNho" +
            "Ef9gvTl2M7K45HRQixS5/8npkqf8JTrlOA6KWXMEjqjLgytXfgZKej7/wE86HJ6tAU0el4vOK6X+Is" +
            "ZAnxvABhLOCjBTWFCCTveghgoSUXd/VFsgOsV4kxRUtdCRQpSvxJ7rH3L6HR/L3IHP1EgMNtYeeIIfb" +
            "2fDIWqpmwfyJaAhuX/bU8Pcut/4wIDAQAB";

    public static final String SKU_LIFETIME = "lifetime_sub";
    public static final String SKU_1_MONTH = "1_month_sub";
    public static final String SKU_6_MONTH = "6_month_sub";
    public static final String SKU_12_MONTH = "12_month_sub";

    public Map<String, SkuDetails> subscriptions;

    private boolean isPro = false;

    public boolean isPro(Context context) {
        isPro = callSavedPreferences1("purchase_status",context);
        return isPro;
    }

    public void setPro(boolean pro,Context context) {
        savePreferences1("purchase_status",pro,context);
        isPro = pro;
    }

    public Map<String, SkuDetails> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Map<String, SkuDetails> subscriptions) {
        Map<String, SkuDetails> map = new HashMap<>();
        map.putAll(subscriptions);
        this.subscriptions = map;
    }

    private static final BillingManager instance = new BillingManager();

    public static BillingManager getInstance() {
        return instance;
    }

    private BillingManager() {
    }


    public static void savePreferences1(String key, Boolean value,
                                        final Context context) {

        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putBoolean(key, value);

        editor.commit();
    }

    public static Boolean callSavedPreferences1(String key, Context context) {

        SharedPreferences sharedPreferences = PreferenceManager

                .getDefaultSharedPreferences(context);

        Boolean name = sharedPreferences.getBoolean(key, false);

        return name;
    }
}