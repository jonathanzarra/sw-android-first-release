package org.strategywiki.database.column;

import android.support.annotation.NonNull;

import org.strategywiki.page.Namespace;

public class NamespaceColumn extends CodeEnumColumn<Namespace> {
    public NamespaceColumn(@NonNull String tbl, @NonNull String name) {
        super(tbl, name, Namespace.CODE_ENUM);
    }
}
