package org.strategywiki.dataclient.page;

import android.support.annotation.NonNull;

import org.strategywiki.page.Page;
import org.strategywiki.page.Section;

import java.util.List;

/**
 * Gson POJI for loading remaining page content.
 */
public interface PageRemaining {
    void mergeInto(Page page);
    @NonNull List<Section> sections();
}
