package org.strategywiki.feed.ads;

import android.support.annotation.NonNull;

import org.strategywiki.feed.model.Card;
import org.strategywiki.feed.model.CardType;

public class AdsCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.ADS;
    }
}
