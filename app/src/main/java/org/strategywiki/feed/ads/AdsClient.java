package org.strategywiki.feed.ads;

import org.strategywiki.dataclient.WikiSite;
import org.strategywiki.feed.dataclient.DummyClient;
import org.strategywiki.feed.model.Card;

public class AdsClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new AdsCard();
    }
}
