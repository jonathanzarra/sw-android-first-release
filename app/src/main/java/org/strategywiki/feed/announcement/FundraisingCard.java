package org.strategywiki.feed.announcement;

import android.support.annotation.NonNull;

import org.strategywiki.feed.model.CardType;

public class FundraisingCard extends AnnouncementCard {

    public FundraisingCard(@NonNull Announcement announcement) {
        super(announcement);
    }

    @NonNull @Override public CardType type() {
        return CardType.FUNDRAISING;
    }
}
