package org.strategywiki.feed.mainpage;

import android.support.annotation.NonNull;

import org.strategywiki.feed.model.Card;
import org.strategywiki.feed.model.CardType;

public class MainPageCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.MAIN_PAGE;
    }
}
