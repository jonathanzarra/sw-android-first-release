package org.strategywiki.feed.mainpage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import org.strategywiki.R;
import org.strategywiki.WikipediaApp;
import org.strategywiki.feed.view.FeedAdapter;
import org.strategywiki.feed.view.StaticCardView;
import org.strategywiki.history.HistoryEntry;
import org.strategywiki.page.PageTitle;

public class MainPageCardView extends StaticCardView<MainPageCard> {
    public MainPageCardView(@NonNull Context context) {
        super(context);
    }

    @Override public void setCard(@NonNull final MainPageCard card) {
        super.setCard(card);
        setTitle(getString(R.string.view_main_page_card_title));
        setSubtitle(getString(R.string.view_main_page_card_subtitle));
        setIcon(R.drawable.icon_feed_today);
    }

    @Override public void setCallback(@Nullable FeedAdapter.Callback callback) {
        super.setCallback(callback);
        setOnClickListener(new CallbackAdapter());
    }

    private class CallbackAdapter implements OnClickListener {
        @NonNull private WikipediaApp app = WikipediaApp.getInstance();

        @Override
        public void onClick(View view) {
            if (getCallback() != null && getCard() != null) {
                PageTitle title = new PageTitle("StrategyWiki:Categories", app.getWikiSite());
                getCallback().onSelectPage(getCard(),
                        new HistoryEntry(title, HistoryEntry.SOURCE_FEED_MAIN_PAGE));
            }
        }
    }
}
//adding this just so i can push to bitbucket
