package org.strategywiki.feed.mainpage;

import org.strategywiki.dataclient.WikiSite;
import org.strategywiki.feed.dataclient.DummyClient;
import org.strategywiki.feed.model.Card;

public class MainPageClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new MainPageCard();
    }
}
