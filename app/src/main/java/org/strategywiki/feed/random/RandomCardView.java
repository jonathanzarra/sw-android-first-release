package org.strategywiki.feed.random;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import org.strategywiki.R;
import org.strategywiki.WikipediaApp;
import org.strategywiki.concurrency.CallbackTask;
import org.strategywiki.dataclient.restbase.page.RbPageSummary;
import org.strategywiki.feed.view.FeedAdapter;
import org.strategywiki.feed.view.StaticCardView;
import org.strategywiki.history.HistoryEntry;
import org.strategywiki.page.PageTitle;
import org.strategywiki.random.RandomArticleIdTask;
import org.strategywiki.random.RandomSummaryClient;
import org.strategywiki.readinglist.page.database.ReadingListPageDao;
import org.strategywiki.util.log.L;

import retrofit2.Call;

public class RandomCardView extends StaticCardView<RandomCard> {
    public RandomCardView(@NonNull Context context) {
        super(context);
    }

    @Override
    public void setCard(@NonNull RandomCard card) {
        super.setCard(card);
        setTitle(getString(R.string.view_random_card_title));
        setSubtitle(getString(R.string.view_random_card_subtitle));
        setIcon(R.drawable.icon_feed_random);
    }

    @Override
    public void setCallback(@Nullable FeedAdapter.Callback callback) {
        super.setCallback(callback);
        setOnClickListener(new CallbackAdapter());
    }

    private class CallbackAdapter implements OnClickListener {
        @Override
        public void onClick(View view) {
            if (getCallback() != null && getCard() != null) {
                setProgress(true);
                WikipediaApp app = WikipediaApp.getInstance();
                new RandomArticleIdTask(app.getAPIForSite(app.getWikiSite()), getCard().wikiSite()) {
                    @Override
                    public void onBeforeExecute() {
                        super.onBeforeExecute();
                    }

                    @Override
                    public void onFinish(PageTitle result) {
                        super.onFinish(result);
                        setProgress(false);
                        getCallback().onSelectPage(getCard(),
                                new HistoryEntry(result, HistoryEntry.SOURCE_FEED_RANDOM));
                    }

                    @Override
                    public void execute() {
                        super.execute();
                    }

                    @Override
                    public void cancel() {
                        super.cancel();
                        setProgress(false);

                    }
                }.execute();
            }
        }

        private RandomSummaryClient.Callback serviceCallback = new RandomSummaryClient.Callback() {
            @Override
            public void onSuccess(@NonNull Call<RbPageSummary> call, @NonNull PageTitle title) {
                setProgress(false);
                if (getCallback() != null && getCard() != null) {
                    getCallback().onSelectPage(getCard(),
                            new HistoryEntry(title, HistoryEntry.SOURCE_FEED_RANDOM));
                }
            }

            @Override
            public void onError(@NonNull Call<RbPageSummary> call, @NonNull Throwable t) {
                L.w("Failed to get random card from network. Falling back to reading lists.", t);
                getRandomReadingListPage(t);
                setProgress(false);
            }
        };

        private void getRandomReadingListPage(@NonNull final Throwable throwableIfEmpty) {
            ReadingListPageDao.instance().randomPage(new CallbackTask.Callback<PageTitle>() {
                @Override
                public void success(@Nullable PageTitle title) {
                    if (getCallback() != null && getCard() != null) {
                        if (title != null) {
                            getCallback().onSelectPage(getCard(),
                                    new HistoryEntry(title, HistoryEntry.SOURCE_FEED_RANDOM));
                        } else {
                            getCallback().onError(throwableIfEmpty);
                        }
                    }
                }
            });
        }
    }
}
