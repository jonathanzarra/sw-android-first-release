package org.strategywiki.feed.random;

import org.strategywiki.dataclient.WikiSite;
import org.strategywiki.feed.dataclient.DummyClient;
import org.strategywiki.feed.model.Card;

public class RandomClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new RandomCard(wiki);
    }
}
