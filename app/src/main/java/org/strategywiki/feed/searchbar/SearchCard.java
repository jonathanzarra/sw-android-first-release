package org.strategywiki.feed.searchbar;

import android.support.annotation.NonNull;

import org.strategywiki.feed.model.Card;
import org.strategywiki.feed.model.CardType;

public class SearchCard extends Card {
    @NonNull @Override public CardType type() {
        return CardType.SEARCH_BAR;
    }
}
