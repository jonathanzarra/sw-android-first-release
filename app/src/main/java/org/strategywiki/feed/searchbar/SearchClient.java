package org.strategywiki.feed.searchbar;

import org.strategywiki.dataclient.WikiSite;
import org.strategywiki.feed.dataclient.DummyClient;
import org.strategywiki.feed.model.Card;

public class SearchClient extends DummyClient {
    @Override public Card getNewCard(WikiSite wiki) {
        return new SearchCard();
    }
}
