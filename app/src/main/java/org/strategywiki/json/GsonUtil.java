package org.strategywiki.json;

import android.net.Uri;
import android.support.annotation.VisibleForTesting;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.strategywiki.dataclient.WikiSite;
import org.strategywiki.page.Namespace;
import org.strategywiki.readinglist.sync.RemoteReadingListPageTypeAdapter;
import org.strategywiki.zero.ZeroConfig;

import static org.strategywiki.readinglist.sync.RemoteReadingLists.RemoteReadingListPage;

public final class GsonUtil {
    private static final String DATE_FORMAT = "MMM dd, yyyy HH:mm:ss";

    private static final GsonBuilder DEFAULT_GSON_BUILDER = new GsonBuilder()
            .setDateFormat(DATE_FORMAT)
            .registerTypeHierarchyAdapter(Uri.class, new UriTypeAdapter().nullSafe())
            .registerTypeHierarchyAdapter(Namespace.class, new NamespaceTypeAdapter().nullSafe())
            .registerTypeAdapter(WikiSite.class, new WikiSiteTypeAdapter().nullSafe())
            .registerTypeAdapter(ZeroConfig.class, new ZeroConfigTypeAdapter().nullSafe())
            .registerTypeAdapter(RemoteReadingListPage.class, new RemoteReadingListPageTypeAdapter().nullSafe())
            .registerTypeAdapterFactory(new RequiredFieldsCheckOnReadTypeAdapterFactory());

    private static final Gson DEFAULT_GSON = DEFAULT_GSON_BUILDER.create();

    public static Gson getDefaultGson() {
        return DEFAULT_GSON;
    }

    @VisibleForTesting
    public static GsonBuilder getDefaultGsonBuilder() {
        return DEFAULT_GSON_BUILDER;
    }

    private GsonUtil() { }
}
