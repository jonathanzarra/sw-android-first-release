package org.strategywiki.model;

public interface EnumCode {
    int code();
}
