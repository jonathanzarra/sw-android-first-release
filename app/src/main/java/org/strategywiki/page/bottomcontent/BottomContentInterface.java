package org.strategywiki.page.bottomcontent;

import org.strategywiki.page.PageTitle;

public interface BottomContentInterface {

    void hide();
    void beginLayout();
    PageTitle getTitle();
    void setTitle(PageTitle newTitle);

}
