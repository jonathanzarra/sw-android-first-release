package org.strategywiki.page.tabs;

import android.support.annotation.NonNull;

import org.strategywiki.model.BaseModel;
import org.strategywiki.page.PageBackStackItem;

import java.util.ArrayList;
import java.util.List;

public class Tab extends BaseModel {
    @NonNull private final List<PageBackStackItem> backStack = new ArrayList<>();

    @NonNull
    public List<PageBackStackItem> getBackStack() {
        return backStack;
    }
}
