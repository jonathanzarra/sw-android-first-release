package org.strategywiki.readinglist.page.database;

import android.database.Cursor;
import android.support.annotation.NonNull;

import org.strategywiki.database.async.AsyncTable;
import org.strategywiki.database.contract.ReadingListPageContract;
import org.strategywiki.database.http.HttpRow;
import org.strategywiki.database.http.HttpStatus;
import org.strategywiki.readinglist.page.ReadingListPageRow;

public class ReadingListPageHttpTable
        extends AsyncTable<HttpStatus, ReadingListPageRow, HttpRow<ReadingListPageRow>> {
    private static final int DATABASE_VERSION = 12;

    public ReadingListPageHttpTable() {
        super(ReadingListPageContract.TABLE_HTTP, ReadingListPageContract.Http.URI,
                ReadingListPageContract.HTTP_COLS);
    }

    @Override public HttpRow<ReadingListPageRow> fromCursor(@NonNull Cursor cursor) {
        return ReadingListPageContract.HTTP_COLS.val(cursor);
    }

    @Override protected int getDBVersionIntroducedAt() {
        return DATABASE_VERSION;
    }
}
