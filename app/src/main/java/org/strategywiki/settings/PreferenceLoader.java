package org.strategywiki.settings;

/*package*/ interface PreferenceLoader {
    void loadPreferences();
}
