package org.strategywiki.edit.richtext;

import android.os.Parcelable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.strategywiki.richtext.URLSpanBoldNoUnderline;
import org.strategywiki.test.TestParcelUtil;
import org.strategywiki.test.TestRunner;

@RunWith(TestRunner.class) public class URLSpanBoldNoUnderlineTest {
    @Test public void testCtorParcel() throws Throwable {
        Parcelable subject = new URLSpanBoldNoUnderline("url");
        TestParcelUtil.test(subject);
    }
}
